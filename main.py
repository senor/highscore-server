from flask import Flask, jsonify, request, json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, inspection
import os
import binascii
from dotenv import load_dotenv

load_dotenv()
LISTEN_ADDRESS = os.getenv("LISTEN_ADDRESS", "127.0.0.1")
LISTEN_PORT = os.getenv("LISTEN_PORT", "5000")
DB_HOST = os.getenv("DB_HOST", "127.0.0.1")
DB_PORT = os.getenv("DB_PORT", "5432")
DB_USER = os.getenv("DB_USER", "user")
DB_PASS = os.getenv("DB_PASS", "pass")

db = SQLAlchemy()

app = Flask(__name__)

#SQLAlchemy
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://" + DB_USER + ":" + DB_PASS + "@" + DB_HOST + ":" + DB_PORT + "/highscoredb"
db.init_app(app)

class Serializer(object):

    def serialize(self):
        return {c: getattr(self, c) for c in inspection.inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]

class Score(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, nullable=False)
    game = db.Column(db.String, nullable=False)
    score = db.Column(db.Integer, nullable=False)
    time = db.Column(db.DateTime(timezone=True), onupdate=func.now())

    def serialize(self):
        d = Serializer.serialize(self)
        return d

with app.app_context():
    db.create_all()


def _encode(string):
    encoded = ""
    for char in string:
        encoded += "%" + binascii.hexlify(bytes(char,"ascii")).decode("utf-8")
    return encoded

def _decode(string):
    decoded = ""
    for char in string.split("%")[1:]:
        decoded += binascii.unhexlify(char).decode("utf-8")
    return decoded

#Routes
@app.route("/")
def hello_world():
    return "Hello World"

# Highscore for a game
@app.route("/score/<game>", methods = ["GET"])
def game_highscore(game):
    limit = request.args.get('limit', default = 10, type = int)

    scores = db.session.execute(db.select(Score)
                .where(Score.game == game)
                .order_by(Score.score)
                .order_by(Score.time)
                .limit(limit)
                ).scalars()

    if scores:
        scores_dict = Serializer.serialize_list(scores.all())
    else:
        scores_dict = [{}]
    
    return _encode(json.dumps(scores_dict))
    

# Highscore for scores in a game
@app.route("/score/<game>/<user>", methods = ["GET"])
def user_highscore(game, user):

    score = db.session.execute(db.select(Score).where(
        Score.game == game,
        Score.username == user
        )).scalar()

    if not score:
        score_dict = {}
    else:
        score_dict = score.serialize()
    return _encode(json.dumps(score_dict))
    
@app.route("/score/<game>/<user>/<int:new_score>")
def set_score_highscore(game, user, new_score):
    _update = False

    score = db.session.execute(db.select(Score).where(
        Score.game == game,
        Score.username == user
        )).scalar()

    # no scores exists
    if not score:
        print("Adding new score")
        score = Score(
                username=user,
                game=game,
                score=new_score
            )
        db.session.add(score)
        db.session.commit()
        _update = True
         
    # new highscore
    elif score and score.score < new_score:
        score.score = new_score
        db.session.commit()
        _update = True

    score_dict = score.serialize()
    score_dict["updated"] = _update
    return _encode(json.dumps(score_dict))

# get list of games
@app.route("/games")
def get_games():
    games = db.session.execute(
        db.select(Score.game)
        .distinct()
    ).scalars()

    return _encode(json.dumps(games.all()))

if __name__ == '__main__':
   app.run(debug=False, host=LISTEN_ADDRESS, port=LISTEN_PORT)